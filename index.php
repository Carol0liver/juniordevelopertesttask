<?php

require __DIR__ . '/vendor/autoload.php';

use App\Classes\ProcessDelete;
use App\Classes\teste;
use App\Db\DbAux;
//Creating a new database auxiliary
$products = new DbAux();
$process = new ProcessDelete();

include __DIR__ . '/includes/headerPgList.php';
include __DIR__ . '/includes/list.php';
include __DIR__ . '/includes/footer.php';
