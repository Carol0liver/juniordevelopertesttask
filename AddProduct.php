<?php

require __DIR__ . '/vendor/autoload.php';

use App\Classes\ProductGenerator;

//POST VALIDATION
if (
    isset($_POST['sku'], $_POST['name'], $_POST['price'])
    && !empty($_POST['sku'])
    && !empty($_POST['name'])
    && !empty($_POST['price'])
) {
    if (
        isset($_POST['size']) or
        isset($_POST['weight']) or
        isset($_POST['length'], $_POST['width'], $_POST['length'])
    ) {
        if (
            !empty($_POST['size'])
            || !empty($_POST['weight'])
            || (!empty($_POST['width'])
            && !empty($_POST['height'])
            && !empty($_POST['length']))
        ) {
            //Creating a new object ProductGenerator
            $obProductGen = new ProductGenerator();

            //Defining product type
            $obProductGen->getType();

            //Creating a new object of the selected type
            $obProduct = $obProductGen->getInstance();
            $obProduct->setSku($_POST['sku']);
            $obProduct->setName($_POST['name']);
            $obProduct->setPrice($_POST['price']);
            $obProduct->setType($_POST['productType']);
            $obProduct->setWeight($_POST['weight']);
            $obProduct->setSize($_POST['size']);
            $obProduct->setHeight($_POST['height']);
            $obProduct->setWidth($_POST['width']);
            $obProduct->setLength($_POST['length']);

            //Saving product
            $obProduct->save();
        }
    }
}


include __DIR__ . '/includes/headerPgAdd.php';
include __DIR__ . '/includes/form.php';
include __DIR__ . '/includes/footer.php';
