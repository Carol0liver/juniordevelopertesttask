<div id="addPage" class="content">
    <form id="product_form" method="POST">
      <!--Header-->
      <header>
        <a href="/">
          <h2>Product Add</h2>
        </a>

        <div class="header-buttons">

          <button class="button" type="submit">
            Save
          </button>

          <a href="index.php" class="button" id="red">Cancel</a>
        </div>
      </header>

      <div class="line"></div>
      
      <main>
      
      <!--Alert Message-->
      <?php
      $fullUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
      if (strpos($fullUrl, "status=skuduplicate") == true) {
          echo "<div class='alert'><p>Existing SKU. The SKU must be unique.</p></div>";
      }
        ?>

        <!--Section with form fields-->
        <section id="input-product">
          <label class="sr-only">Form to add products</label>

          <label for-id="sku">SKU</label>
          <input type="text" id="sku" name="sku" placeholder="Enter the product SKU" 
          pattern="^[A-Za-z0-9_]{1,15}$" required><br>

          <label for-id="name">Name</label>
          <input type="text" id="name" name="name" placeholder="Enter product name" 
          pattern="[a-zA-Záãâéêíîóôõú0-9-\s]+$" required><br>

          <label for-id="price">Price</label>
          <input type="number" step="any" id="price" name="price" placeholder="Enter product price" 
          pattern="\d+(\.\d{2})?" required><br>
          
          <label for-id="productType">Type</label><br>
          <select id="productType" name="productType" required>
            <option value="" disabled selected>Select product type</option>
            <option id="DVD" value="DVD">DVD</option>
            <option id="Book" value="Book">Book</option>
            <option id="Furniture" value="Furniture">Furniture</option>
          </select>

          <!--Special Attributes-->
          <div id="dvdAtb" style="display:none;">
            <br />
            <h3>Please, provide size:</h3><br />
            <label for-id="name">Size (MB)</label>
            <input type="number" step="any" id="size" name="size" placeholder="Enter product size" 
            pattern="[-+]?[0-9]*[.,]?[0-9]+">
          </div>

          <div id="fntAtb" style="display:none;">
            <br />
            <h3>Please, provide dimensions in HxWxL format:</h3><br />
            <label for-id="name">Height (CM)</label>
            <input type="number" step="any" id="height" name="height" placeholder="Enter product height" 
            pattern="[-+]?[0-9]*[.,]?[0-9]+">
            <label for-id="name">Width (CM)</label>
            <input type="number" step="any" id="width" name="width" placeholder="Enter product width" 
            pattern="[-+]?[0-9]*[.,]?[0-9]+">
            <label for-id="name">Length (CM)</label>
            <input type="number" step="any" id="length" name="length" placeholder="Enter product length" 
            pattern="[-+]?[0-9]*[.,]?[0-9]+">
          </div>

          <div id="bookAtb" style="display:none;">
            <br />
            <h3>Please, provide weight:</h3><br />
            <label for-id="name">Weight (KG)</label>
            <input type="number" step="any" id="weight" name="weight" placeholder="Enter product weight" 
            pattern="[-+]?[0-9]*[.,]?[0-9]+">
          </div>
        </section>
      </main>  
    </form>