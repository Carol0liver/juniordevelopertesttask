<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Product List</title>
  <!-- View Port -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!--css-->
  <link rel="stylesheet" href="styles/global.css">
  <link rel="stylesheet" href="styles/addPage.css">


  <!-- FONTS -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Poppins:wght@400;500;700&display=swap"
    rel="stylesheet">

  <!--JQUERRY-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

  <!--Dynamically change the #product_form, showing or hiding fields based on type switch-->
  <script>
    $(document).ready(function () {
      $("#productType").change(function () {
        if ($(this).val() == "Book") {
          $("#bookAtb").show(); 
          $("#dvdAtb").hide();
          $("#fntAtb").hide();
          
          //Setting atribute "require" just for visible input field
          document.getElementById("weight").required = true;
          document.getElementById("size").required = false;
          document.getElementById("height").required = false;
          document.getElementById("width").required = false;
          document.getElementById("length").required = false; 

        } else if ($(this).val() == "DVD") {
          $("#dvdAtb").show();
          $("#bookAtb").hide();
          $("#fntAtb").hide();

          //Setting atribute "require" just for visible input field
          document.getElementById("size").required = true;
          document.getElementById("weight").required = false;
          document.getElementById("height").required = false;
          document.getElementById("width").required = false;
          document.getElementById("length").required = false;          

        } else if ($(this).val() == "Furniture") {
          $("#fntAtb").show();
          $("#bookAtb").hide();
          $("#dvdAtb").hide();

          //Setting atribute "require" just for visible input fields
          document.getElementById("height").required = true;
          document.getElementById("width").required = true;
          document.getElementById("length").required = true;
          document.getElementById("weight").required = false;
          document.getElementById("size").required = false;     
        }
      });
    });

  </script>
 
  <!--JQUERRY END-->

</head>

<body>