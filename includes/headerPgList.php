<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Product List</title>
    <!-- View Port -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--css-->
    <link rel="stylesheet" href="styles/global.css" />
    <link rel="stylesheet" href="styles/ProductList.css" />

    <!-- FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Poppins:wght@400;500;700&display=swap"
        rel="stylesheet" />

</head>

<body>
    <div id="listPage" class="content">
    <?php if (isset($_POST['delete-product-btn']) && !empty($_POST['checkbox'])) {
        $process->delete();
    }?>
    <form action="" method="post">

            <!--Header-->
            <header>
                <a href="/">
                    <h2>Product List</h2>
                </a>
                <div class="header-buttons">
                    <a href="AddProduct.php" class="button">ADD</a>

                    <button class="red" id="delete-product-btn" name="delete-product-btn" 
                    type="submit">MASS DELETE</button>
                </div>
            </header>

            <div class="line"></div>
