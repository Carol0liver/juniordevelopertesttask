        <main>

            <!--Section with product list-->
            <section id="products">
                <label class="sr-only">List with products</label>
                <div class="products">
                <!--Creating the product list from database data-->
                <?php
                if ($products->getProducts()) {
                    foreach ($products->getProducts() as $product) {
                        echo '<div class="product-wrapper">';
                        echo '<div class="actions">';
                        echo "<input class='delete-checkbox' type='checkbox'  
                        name='checkbox[]' value='$product[id]' id='checkbox[]' />";
                        echo '</div>';
                        echo '<div class="product-content">';
                        echo '<div class="product">';
                        echo "<p>" . $product['sku'] . "</p>";
                        echo "<p>" . $product['name'] . "</p>";
                        echo "<p>" . $product['price'] . "&nbsp$</p>";

                        //Testing if the referred data exists, then showing
                        $DvdIsNull = is_null($product['size']);
                        if (!$DvdIsNull) {
                            echo "<p><b>Size:&nbsp</b>" . $product['size'] . "&nbspMB</p>";
                        }
                        //Testing if the referred data exists, then showing
                        $BookIsNull = is_null($product['weight']);
                        if (!$BookIsNull) {
                            echo "<p><b>Weight:&nbsp</b>" . $product['weight'] . "&nbspKG</p>";
                        }
                        //Testing if the referred data exists, then showing
                        $FntIsNull = is_null($product['height']);
                        if (!$FntIsNull) {
                            echo "<p><b>Dimension:&nbsp</b>" . $product['height'] . "x"
                            . $product['width'] . "x" . $product['length'] . "</p>";
                        }
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                    }
                //If there is no product in the database, show a message
                } else {
                    echo "<div class='no-products'>";
                    echo "<div class='image-wrapper'>";
                    echo '<img src="images/noproducts.png" alt="No products">';
                    echo "</div>";
                    echo "<p>No products available</p>";
                    echo "<p>Add the first product!</p>";
                    echo "</div>";
                }
                ?>
                </div>
            </section>
        </main>
        </form>