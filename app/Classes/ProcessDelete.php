<?php

namespace App\Classes;

use App\Db\DbAux;

class ProcessDelete
{
    //Create the querry to delete process and send to the DbAux
    public function delete()
    {
        //Creating a new database auxiliary
        $productsDel = new DbAux();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $arr = $productsDel->filter($_POST['checkbox']);

            $sql = 'DELETE FROM products WHERE id IN(' . implode(',', $arr) . ')';

            $productsDel->delProducts($sql);
        }
    }
}
