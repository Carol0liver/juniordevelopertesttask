<?php

namespace App\Classes;

class ProductGenerator
{

    /**
     * Product type name
     *
     * @var string
     */
    public $prdt;

    /**
     * Method responsible for receiving the product type
     *
     * @return void
     */
    public function getType()
    {
        $this->prdt = $_REQUEST['productType'];
    }

    /**
     * Method responsible for creating a new object of the defined type
     *
     * @return object
     */
    public function getInstance()
    {
        $className = 'App\\Entity\\' . $this->prdt;
        return new $className();
    }
}
