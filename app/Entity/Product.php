<?php

namespace App\Entity;

//Abstract "product" class that will serve as a basis to the classes Book, Dvd and Furniture
abstract class Product
{
        /**
         * Unique identity of product
         *
         * @var int
         */
        protected $id;

        /**
         * Stock Keeping Unit
         *
         * @var string
         */
        protected $sku;

        /**
         * Product name
         *
         * @var string
         */
        protected $name;

        /**
         * Product price
         *
         * @var string
         */
        protected $price;

        /**
        * Product type
        *
        * @var string (dvd/book/furniture)
        */
        protected $type;

        protected $height = null;
        protected $width = null;
        protected $length = null;
        protected $weight = null;
        protected $size = null;


    public function setId($id)
    {
        $this->id = $id;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function setLength($length)
    {
        $this->length = $length;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
    /**
     * Method responsible to save a new product
     *
     * @return boolean
     */
    abstract protected function save();
}
