<?php

namespace App\Entity;

use App\Db\DbAux;

class Furniture extends Product
{
    /**
     * Product height
     *
     * @var float
     */
    protected $height;

    /**
     * Product width
     *
     * @var float
     */
    protected $width;

    /**
     * Product length
     *
     * @var float
     */
    protected $length;

    /**
     * Method that sets the product height
     *
     * @param float $height
     * @return void
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * Method that sets the product width
     *
     * @param float $width
     * @return void
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * Method that sets the product length
     *
     * @param float $length
     * @return void
     */
    public function setLength($length)
    {
        $this->length = $length;
    }


     /**
     * Method responsible for save a new product
     *
     * @return boolean
     */
    public function save()
    {
        //Creating a new object responsible for performing the insertion in the database
        $obDatabase = new DbAux();

        //Checking if SKU already exists in the database
        if ($obDatabase->skuExists($this->sku) == false) {
            //If does not, send the data to the method responsible for the insertion
            $obDatabase->setProducts([
                'sku' => $this->sku,
                'name' => $this->name,
                'price' => $this->price,
                'type' => $this->type,
                'height' => $this->height,
                'width' => $this->width,
                'length' => $this->length
            ]);
            header('location: index.php?status=success');
            exit;
        } else {
            //Else, returns to the product addition page with an alert message
            header('location: AddProduct.php?status=skuduplicate');
        }

        //Return success
        return true;
    }
}
