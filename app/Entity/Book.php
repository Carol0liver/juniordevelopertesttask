<?php

namespace App\Entity;

use App\Db\DbAux;

class Book extends Product
{
    /**
     * Product weight
     *
     * @var float
     */
    protected $weight;

    /**
     * Method that sets the product weight
     *
     * @param float $weight
     * @return void
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Method responsible for save a new product
     *
     * @return boolean
     */
    public function save()
    {
        //Creating a new object responsible for performing the insertion in the database
        $obDatabase = new DbAux();

        //Checking if SKU already exists in the database
        if ($obDatabase->skuExists($this->sku) == false) {
            //If does not, send the data to the method responsible for the insertion
            $obDatabase->setProducts([
                'sku' => $this->sku,
                'name' => $this->name,
                'price' => $this->price,
                'type' => $this->type,
                'weight' => $this->weight
            ]);
            header('location: index.php?status=success');
            exit;
        } else {
            //Else, returns to the product addition page with an alert message
            header('location: AddProduct.php?status=skuduplicate');
        }

        //Return success
        return true;
    }
}
