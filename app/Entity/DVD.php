<?php

namespace App\Entity;

use App\Db\DbAux;

class Dvd extends Product
{
    /**
     * Product size
     *
     * @var float
     */
    protected $size;

    /**
     * Method that sets the product size
     *
     * @param float $size
     * @return void
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * Method responsible for save a new product
     *
     * @return void
     */
    public function save()
    {
        //Creating a new object responsible for performing the insertion in the database
        $obDatabase = new DbAux();

        //Checking if SKU already exists in the database
        if ($obDatabase->skuExists($this->sku) == false) {
            //If does not, send the data to the method responsible for the insertion
            $obDatabase->setProducts([
            'sku' => $this->sku,
            'name' => $this->name,
            'price' => $this->price,
            'type' => $this->type,
            'size' => $this->size
            ]);
            header('location: index.php?status=success');
            exit;
        } else {
            //Else, returns to the product addition page with an alert message
            header('location: AddProduct.php?status=skuduplicate');
        }

        //Return success
        return true;
    }
}
