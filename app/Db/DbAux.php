<?php

namespace App\Db;

use PDO;

class DbAux extends Dbh
{
    /**
     * Method responsible for getting products from the database.
     *
     * @return array
     */
    public function getProducts()
    {
        //SQL Querry
        $sql = "SELECT * FROM products ORDER BY
        id DESC";
        //Prepare the SQL statement
        $stmt = $this->connect()->prepare($sql);

        //Execute the querry
        $stmt->execute();

        //Returning data
        while ($result = $stmt->fetchAll()) {
            return $result;
        };
    }

    /**
     * Method responsible for putting data into the database.
     *
     * @param array $values
     * @return void
     */
    public function setProducts($values)
    {
        //Querry data
        $table = "products";
        $fields = array_keys($values);
        $binds  = array_pad([], count($fields), '?');

        //Create the querry
        $sql = 'INSERT INTO ' . $table . ' (' . implode(', ', $fields) . ') 
        VALUES (' . implode(',', $binds) . ')';

        //Prepare the SQL statement
        $stmt = $this->connect()->prepare($sql);

        //Execute the insert
        $stmt->execute(array_values($values));
    }

    /**
     * Method that checks if sku already exists in the database
     *
     * @param string $sku
     * @return boolean
     */
    public function skuExists($sku)
    {
        $skuToLookFor = $sku;

        //SQL query
        $sql = "SELECT COUNT(sku) AS num FROM `products` WHERE sku = :sku";

        //Prepare the SQL statement
        $stmt = $this->connect()->prepare($sql);

        //Bind sku value to the :sku parameter
        $stmt->bindValue(':sku', $skuToLookFor);

        //Execute the verification
        $stmt->execute();

        //Fetch the result
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        //If num is bigger than 0 return true, else return false.
        if ($row['num'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method responsible for executing the exclusion on the database
     *
     * @param string $sql
     * @return boolean
     */
    public function delProducts($sql)
    {
        //Prepare the SQL statement
        $stmt = $this->connect()->prepare($sql);

        //Execute the exclusion
        $stmt->execute();
    }

    //Method to casting type
    public function filter($data)
    {
        $arr = array();
        foreach ($data as $dat) {
            $arr[] = (int)$dat;
        }
        return $arr;
    }
}
