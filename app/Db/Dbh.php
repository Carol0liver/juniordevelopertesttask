<?php

namespace App\Db;

use PDO;

class Dbh
{
    /**
     * Database connection host
     */
    private $host = "localhost";

    /**
     * Database user
     */
    private $user = "root";

    /**
     * Database access password
     */
    private $pwd = "";

    /**
     * Database name
     */
    private $dbName = "scandiweb_test";

    /**
     * Method responsible for creating a database connection
     *
     */
    public function connect()
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
        $pdo = new PDO($dsn, $this->user, $this->pwd);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }
}
